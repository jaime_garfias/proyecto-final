import { LitElement, html, css } from 'lit-element';
import sandbox from '../sandbox';

export class MyFooter extends LitElement {

    constructor(){
        super();
        sandbox.on('agregar-sda',this.agregarSDA.bind(this));
    }

    static styles = css`
        *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }
        footer{
            width: 100%;
            height: auto;
            background-color: #01a5a6;
            padding: 2em;
            color: #fff;
        }
    `;

    render() {
        return html`
            <footer>
                <span>HACKATON FRONT END 2022</span>
            </footer>
        `;
    }

    agregarSDA(){
        console.log("Escuche");
    }

}
customElements.define('my-footer', MyFooter);