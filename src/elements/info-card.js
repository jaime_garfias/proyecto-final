import { LitElement, html, css } from 'lit-element';
import sandbox from '../sandbox';

export class InfoCard extends LitElement {

    constructor(){
        super();
        this.sda = "";
        this.integrantes = "";
        this.falta = "";
        this.fliberacion = "";
        this.descripcion = "";
        this.id = 1;
        this.count = 0;
        sandbox.on('show-information-project',this.accionEscucha.bind(this));
    }

    static get properties() {
        return {
            id: {type: Number},
            propName: { type: String },
            sda: { type: String },
            descripcion: {type: String},
            integrantes: { type: String },
            falta: { type: String },
            fliberacion: { type: String },
            userId: {type: Number},
            count: {type: Number},
        };
    }

    static styles = css`
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    .info-card{
        background-color: #fff;
        padding: 1.5em;
    }
    .contenedor-campo{
        display: flex;
        flex-direction: column;
    }
    .contenedor-campo{
        margin-bottom: 2em;
    }
    .contenedor-campo label{
        margin-bottom: .5em;
    }
    .contenedor-campo input{
        border-bottom: 1px solid #0b4169;
        border-top: none;
        border-left: none;
        border-right: none;
        padding: .5em;
    }
    .contenedor-checklists h3, .item-checklist{
        margin-bottom: 1.5em;
    }
    button{
        background-color: #0b4169;
        border: none;
        padding: .8em;
        cursor: pointer;
        color: #fff;
        font-size: 1.2em;
    }
    `;

    render() {
        return html`
            <div class="info-card">
                <div class="contenedor-campos">
                    <div class="contenedor-campo">
                        <label>SDATOOL</label>
                        <input type="text" value="${this.sda}" @input="${this.actualizarSDA}">
                    </div>
                    <div class="contenedor-campo">
                        <label>Descipción</label>
                        <input type="text" value="${this.descripcion}" @input="${this.actualizarDescripcion}">
                    </div>                    
                    <div class="contenedor-campo">
                        <label>INTEGRANTES DEL EQUIPO</label>
                        <input type="text" value="${this.integrantes}" @input="${this.actualizarIntegrantes}">
                    </div>   
                    <div class="contenedor-campo">
                        <label>FECHA DE ALTA DE INICIATIVA</label>
                        <input type="text" value="${this.falta}" @input="${this.actualizarFAlta}">
                    </div>             
                    <div class="contenedor-campo">
                        <label>FECHA DE LIBERACIÓN</label>
                        <input type="text" value="${this.fliberacion}" @input="${this.actualizarFLiberacion}">
                    </div>
                </div>
                <div class="contenedor-checklists">
                    <h3>Administración del checklist tecnológico</h3>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>DEFINICIÓN FUNCIONAL INTEGRAL Y TÉCNICA INTEGRAL</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>CÓDIGO FUENTE ALMACENADO EN REPOSITORIO CORPORATIVO</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>ANÁLISIS ESTÁTICO DE CÓDIGO FUENTE</label>
                    </div>
                    <div class="item-checklist">
<<<<<<< HEAD
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>PRUEBAS DE SEGURIDAD APLICATIVA	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>VERIFICACIÓN DE HACKING ÉTICO	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>PRUEBAS FUNCIONALES Y TÉCNICAS (PREVIAS)	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>PRUEBAS DE RENDIMIENTO	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>PRUEBAS FUNCIONALES Y TÉCNICAS (POSTERIORES)	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>GESTIÓN DEL CAMBIO (CADA FASE)	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>READY FOR OPERATION	</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>PERFILADO DE USUARIO DELEGADO IAM	</label>
=======
                        <input type="checkbox">
                        <label>PRUEBAS DE SEGURIDAD APLICATIVA  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>VERIFICACIÓN DE HACKING ÉTICO  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS FUNCIONALES Y TÉCNICAS (PREVIAS) </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS DE RENDIMIENTO </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS FUNCIONALES Y TÉCNICAS (POSTERIORES) </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>GESTIÓN DEL CAMBIO (CADA FASE) </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>READY FOR OPERATION  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PERFILADO DE USUARIO DELEGADO IAM  </label>
>>>>>>> f963dc781fcfd89d3ca3cd29ee4247476b922ca8
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>CREACIÓN O MODIFICACIÓN DE TRANSACCIONES/PERFILES IAM</label>
                    </div>
                    <div class="item-checklist">
<<<<<<< HEAD
                        <input type="checkbox" @click="${this.validaCheck}">
                        <label>MONITORIZACIÓN DE FRAUDE	</label>
=======
                        <input type="checkbox">
                        <label>MONITORIZACIÓN DE FRAUDE </label>
>>>>>>> f963dc781fcfd89d3ca3cd29ee4247476b922ca8
                    </div>                    
                </div>
                <div class="contenedor-botones">
                    <button @click="${this.agregarSDA}">Agregar/Modificar</button>
                    <a href="./home"><button>Regresar</button></a>
                </div>
            </div>
        `;
    }

    actualizarSDA(e){
        this.sda = e.target.value;
    }
    actualizarDescripcion(e){
        this.descripcion = e.target.value;
    }
    actualizarIntegrantes(e){
        this.integrantes = e.target.value;
    }
    actualizarFAlta(e){
        this.falta = e.target.value;
    }
    actualizarFLiberacion(e){
        this.fliberacion = e.target.value;
    }

    agregarSDA(){
        console.log("Agregar SDA");
        let body = {};
        let image = {};
        let progress = {};
        let relatedDates = {};
        body.userId = this.userId;
        body.sda = this.sda;
        body.description = this.descripcion;
        image.src = "project.jpg";
        body.image = image;
        progress.actual = this.count;
        progress.total = "13";
        body.progress = progress;
        relatedDates.start = this.falta;
        relatedDates.end = this.fliberacion;
        body.relatedDates = relatedDates;
        body.team = this.integrantes;
        let checklist = [

            {
                "id": "DYD-01",
                "name": "DEFINICIÓN FUNCIONAL INTEGRAL Y TÉCNICA INTEGRAL\t",
                "ready": false
                },
                {
                "id": "DYD-02",
                "name": "CÓDIGO FUENTE ALMACENADO EN REPOSITORIO CORPORATIVO\t",
                "ready": false
                },
                {
                "id": "DYD-03",
                "name": "ANÁLISIS ESTÁTICO DE CÓDIGO FUENTE\t",
                "ready": false
                },
                {
                "id": "DYD-04",
                "name": "PRUEBAS DE SEGURIDAD APLICATIVA\t",
                "ready": false
                },
                {
                "id": "DYD-05",
                "name": "VERIFICACIÓN DE HACKING ÉTICO\t",
                "ready": false
                },
                {
                "id": "DYD-06",
                "name": "PRUEBAS FUNCIONALES Y TÉCNICAS (PREVIAS)\t",
                "ready": false
                },
                {
                "id": "DYD-07",
                "name": "PRUEBAS DE RENDIMIENTO\t",
                "ready": false
                },
                {
                "id": "DYD-08",
                "name": "PRUEBAS FUNCIONALES Y TÉCNICAS (POSTERIORES)\t",
                "ready": false
                },
                {
                "id": "DYD-09",
                "name": "GESTIÓN DEL CAMBIO (CADA FASE)\t",
                "ready": false
                },
                {
                "id": "DYD-10",
                "name": "READY FOR OPERATION\t",
                "ready": false
                },
                {
                "id": "DYD-11",
                "name": "PERFILADO DE USUARIO DELEGADO IAM\t",
                "ready": false
                },
                {
                "id": "DYD-12",
                "name": "CREACIÓN O MODIFICACIÓN DE TRANSACCIONES/PERFILES IAM\t",
                "ready": false
                },
                {
                "id": "DYD-13",
                "name": "MONITORIZACIÓN DE FRAUDE\t",
                "ready": false
                }

        ];
        body.checklist = checklist;
        var json = JSON.stringify(body);
        console.log(json);

        let url = "http://localhost:3005/projects";

        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
        xhr.onload = function () {
            var users = JSON.parse(xhr.responseText);
            if (xhr.readyState == 4 && xhr.status == "201") {
                console.table(users);	
            } else {
                console.error(users);
            }
        }
        xhr.send(json);  

    }
<<<<<<< HEAD

    validaCheck(e){
        if(e.target.checked){
            this.count ++;
        }else{
            this.count --;
        }
    }

    accionEscucha(){
        console.log("Escuche: ");
    }

=======
>>>>>>> f963dc781fcfd89d3ca3cd29ee4247476b922ca8
}
customElements.define('info-card', InfoCard);