import { LitElement, html, css } from 'lit-element';
import sandbox from '../sandbox';

export class InfoCard extends LitElement {

    constructor(){
        super();
    }

    static get properties() {
        return {
            propName: { type: String },
            crearTarjeta: {}
        };
    }

    static styles = css`
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    .info-card{
        background-color: #fff;
        padding: 1.5em;
    }
    .contenedor-campo{
        display: flex;
        flex-direction: column;
    }
    .contenedor-campo{
        margin-bottom: 2em;
    }
    .contenedor-campo label{
        margin-bottom: .5em;
    }
    .contenedor-campo input{
        border-bottom: 1px solid #0b4169;
        border-top: none;
        border-left: none;
        border-right: none;
        padding: .5em;
    }
    .contenedor-checklists h3, .item-checklist{
        margin-bottom: 1.5em;
    }
    button{
        background-color: #0b4169;
        border: none;
        padding: .8em;
        cursor: pointer;
        color: #fff;
        font-size: 1.2em;
    }
    `;

    render() {
        return html`
            <div class="info-card">
                <div class="contenedor-campos">
                    <div class="contenedor-campo">
                        <label>SDATOOL</label>
                        <input type="text">
                    </div>
                    <div class="contenedor-campo">
                        <label>INTEGRANTES DEL EQUIPO</label>
                        <input type="text">
                    </div>   
                    <div class="contenedor-campo">
                        <label>FECHA DE ALTA DE INICIATIVA</label>
                        <input type="text">
                    </div>             
                    <div class="contenedor-campo">
                        <label>FECHA DE LIBERACIÓN</label>
                        <input type="text">
                    </div>
                </div>
                <div class="contenedor-checklists">
                    <h3>Administración del checklist tecnológico</h3>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>DEFINICIÓN FUNCIONAL INTEGRAL Y TÉCNICA INTEGRAL</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>CÓDIGO FUENTE ALMACENADO EN REPOSITORIO CORPORATIVO</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>ANÁLISIS ESTÁTICO DE CÓDIGO FUENTE</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS DE SEGURIDAD APLICATIVA  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>VERIFICACIÓN DE HACKING ÉTICO  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS FUNCIONALES Y TÉCNICAS (PREVIAS) </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS DE RENDIMIENTO </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PRUEBAS FUNCIONALES Y TÉCNICAS (POSTERIORES) </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>GESTIÓN DEL CAMBIO (CADA FASE) </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>READY FOR OPERATION  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>PERFILADO DE USUARIO DELEGADO IAM  </label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>CREACIÓN O MODIFICACIÓN DE TRANSACCIONES/PERFILES IAM</label>
                    </div>
                    <div class="item-checklist">
                        <input type="checkbox">
                        <label>MONITORIZACIÓN DE FRAUDE </label>
                    </div>                    
                </div>
                <div class="contenedor-botones">
                    <button @click="${this.agregarSDA}">Agregar/Modificar</button>
                    <a href="./home"><button>Regresar</button></a>
                </div>
            </div>
        `;
    }

    agregarSDA(){
        console.log("Agregar SDA");
        sandbox.dispatch('agregar-sda',{
        },this);
    }
}
customElements.define('info-card', InfoCard);