import { LitElement, html } from 'lit-element';
import sandbox from '../../sandbox';
import './checklist-login/checklist-login.js';
import './checklist-register/checklist-register.js';
import {Router} from '@vaadin/router';

export class LoginApp extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html``;
    }
}
customElements.define('login-app', LoginApp);