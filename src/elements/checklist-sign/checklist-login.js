import { LitElement, html, css } from 'lit';
import sandbox from '../../sandbox';

export class ChecklistLogin extends LitElement {
  static get properties() {
    return {
      host: { type: String },
      resource: { type: String },
      email: { type: String },
      password: { type: String }
    };
  }

  static get styles() {
    return css`
      :host {
        margin: 0;
        padding: 0;
        background-color: #FFFFFF;
        width: 200vh;
        height: 100vh;
      }
      #login .container #login-row #login-column #login-box {
        margin-top: 100px;
        margin-bottom: 100px;
        max-width: 600px;
        height: 320px;
        border: 1px solid #9C9C9C;
        background-color: #EAEAEA;
      }
      #login .container #login-row #login-column #login-box #login-form {
        padding: 20px;
      }
      #login .container #login-row #login-column #login-box #login-form #register-link {
        margin-top: -85px;
      }
    `;
  }

  constructor() {
    super();
    this.email = '';
    this.password = '';
    this.host = window.AppConfig.host;
    this.resource = '/login';
    this.checkLogin();
  }

  doLogin(e) {
    e.preventDefault();
    const data = {
      email: this.email,
      password: this.password
    }
    const url = this.host + this.resource;
    const json = JSON.stringify(data);
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
    xhr.onload = () => {
      let response = JSON.parse(xhr.responseText);
      if (xhr.readyState === 4 && xhr.status === 200) {
        sandbox.dispatch('login-ok', response, this);
      } else {
        sandbox.dispatch('login-error', response, this);
      }
    }
    xhr.send(json);        
  }

  render() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <div id="login">
      <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
          <div id="login-column" class="col-md-6">
            <div id="login-box" class="col-md-12">
              <form id="login-form" class="form">
                <h3 class="text-center text-info">Iniciar sesi&oacute;n</h3>
                <div class="form-group">
                  <label for="email" class="text-info">Email:</label><br>
                  <input type="text" name="email" id="email" class="form-control" @input="${this.updateEmail}" value="${this.email}" required>
                </div>
                <div class="form-group">
                  <label for="password" class="text-info">Contrase&ntilde;a:</label><br>
                  <input type="password" name="password" id="password" class="form-control" @input="${this.updatePassword}" value="${this.password}" required>
                </div>
                <div class="form-group">
                  <br>
                  <button class="btn btn-info btn-md" @click="${this.doLogin}">Iniciar</button>
                </div>
                <div id="register-link" class="text-right">
                  <a href="/register-user" class="text-info">Registrate aquí</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  updateEmail(e) {
    this.email = e.target.value;
  }

  updatePassword(e) {
    this.password = e.target.value;
  }

  checkLogin() {
    let auth = sessionStorage.getItem("accessToken");
    let id = sessionStorage.getItem("id");
    if (auth && id) {
      const url = window.AppConfig.host + "/users/" + id;
      const xhr = new XMLHttpRequest();
      xhr.onload = () => {
        if (xhr.status === 200) {
          let response = JSON.parse(xhr.responseText);
          sandbox.dispatch('login-ok', { user: response }, this)
        } else {
          let response = JSON.parse(xhr.responseText);
          sandbox.dispatch('register-error', response, this);
        }
      }
      xhr.open("GET", url);
      xhr.setRequestHeader('Authorization', 'Bearer ' + auth);
      xhr.send();
    }
  }
}
customElements.define('checklist-login', ChecklistLogin);