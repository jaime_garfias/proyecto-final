import { LitElement, html, css } from 'lit-element';  
import './checklist-menu.js';
import sandbox from '../../sandbox.js';

class ChecklistHeader extends LitElement {

  static get styles() {
    return [
      css`
        #menu-check{
          background-color: #0b4169 !important;
        }
      `
    ];
  }

  static get properties() {
    return {
      userName: { type: String, attribute: 'user-name' },
      initials: { type: String, attribute: 'initials' },
      title: { type: String, attribute: 'title' },
      user: { type: Object }
    };
  }

  constructor() {
    super();
    this.userName = '';
    this.initials = '';
    this.title = 'Checklist de validación técnica';
    this.user = {};
    sandbox.on('send-user-information', this.readUserInformation.bind(this));
  }
  
  render() { 
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class="d-flex justify-content-between bg-dark text-white" id="menu-check">
        ${this.user.initials ? html `
          <div class="p-2 d-flex">
            <span class="p-2 rounded-circle bg-info">
              <strong>${this.user.initials}</strong>
            </span>
            <label class="p-2">${this.user.name}</label>
          </div>`: html `<span>&nbsp;</span>`
        }
        <div class="p-2">
          <h3>${this.title}</h3>
        </div>
        ${this.user.initials ? html `
          <div class="p-2">
            <button type="button" class="btn btn-link" @click="${this.fireEventCloseSession}">Cerrar sesión</button>
          </div>`: html `<span>&nbsp;</span>` }
      </div>
      ${this.user.initials ? html `<checklist-menu></checklist-menu>`: html `<span>&nbsp;</span>`}
    `;
  } 

  fireEventCloseSession() {
    this.user = {};
    sandbox.dispatch('session-close', {}, this);
  }

  readUserInformation(e) {
    this.user = e.user;
  }
}  

customElements.define('checklist-header', ChecklistHeader);