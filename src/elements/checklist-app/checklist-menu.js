import { LitElement, html, css } from 'lit-element';  
import sandbox from '../../sandbox';

class ChecklistMenu extends LitElement {

  static get styles() {
    return [
      css`
        :host {
            margin-top: 80px;
        }
      `
    ];
  }

  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }
  
  render() { 
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <div class="d-flex justify-content-between bg-dark">
        <div class="p-2">
        <a href="/" class="btn btn-link"">Iniciio</a>
          <a href="agregar"><button type="button" class="btn btn-link" @click="${this.fireEventCreateSdaChecklist}">Agregar</button></a>
        </div>
      </div>
    `;
  } 
  
  fireEventCreateSdaChecklist() {
    this.dispatchEvent(new CustomEvent('create-sda-checklist', {}));
  }


}  

customElements.define('checklist-menu', ChecklistMenu);