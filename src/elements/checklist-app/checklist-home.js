import { LitElement, html, css } from 'lit-element';
import './checklist-header.js';
import '../list-items/list-items.js';
import '../info-card.js';
import '../my-footer.js';

export class ChecklistHome extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`
            <checklist-header></checklist-header>
            <list-items style="display:block"></list-items>
            <info-card style="display:none"></info-card>
            <my-footer></my-footer>
        `;
    }
}
customElements.define('checklist-home', ChecklistHome);