import { LitElement, html, css } from 'lit-element';

export class ListItems extends LitElement {

    static styles = css`
        *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }
        .contenedor-intems{
            width: 100%;
            height: auto;
            position: relative;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: space-evenly;
            padding: 2em 0em 2em 0em;
        }
        .elemento{
            width: 400px;
            height: 400px;
            background-color: #fff;
            margin: 1em;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
        }
        .elemento:hover{
            border-bottom: 8px solid #01a5a6;
        }
    `;

    render() {
        return html`
            <div class="contenedor-intems">
                <div class="elemento"></div>
                <div class="elemento"></div>
                <div class="elemento"></div>
                <div class="elemento"></div>
                <div class="elemento"></div>
                <div class="elemento"></div>
                <div class="elemento"></div>
            </div>
        `;
    }
}
customElements.define('list-items', ListItems);