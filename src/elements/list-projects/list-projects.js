import { LitElement, html, css } from 'lit-element';
import './item-project.js';
import $ from '../../../utils/ajax.js';
import sandbox from '../../sandbox.js';

class ListProjects  extends LitElement {

	static get properties() {
        return {
            host: { type:String },
            projects: { type:Array }
        };
    }

    static get styles() {
        return css`
          :host {
            margin: 0;
            
            background-color: #FFFFFF;
            width: 200vh;
            height: 100vh;
          }
          
          .no-projects {
            background-color: #FFF;
            height: 120px;
            border-radius: 15px;
            margin: 50px;
            padding: 2em;
            text-align: center;
            font-size: 1.2em;
        }
        `;
      }


  constructor() {
	super();
    this.host = window.AppConfig.host;
    this.projects = [];
    sandbox.on('delete-project', this.deleteProject.bind(this));
  }

    connectedCallback() {
        super.connectedCallback();
        this.getProjects();
    }

  render() {
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        ${this.projects.length === 0 ? 
            html `<div class"no-projects" style="background-color: #FFF; height: 120px; border-radius: 15px;
                margin: 50px; padding: 2em; text-align: center; font-size: 1.2em;">
                Por el momento no tienes proyectos registrados<br>Presiona <b>Agregar</b> para comenzar
            </div>` : 
            this.projects.map(function(item){
                return html `<item-project
                    id="${item.id}",
                    sda="${item.sda}"
                    name="${item.name}"
                    image="${item.image.src}"
                    description="${item.description}"  
                    .progress="${item.progress}">
                </item-project>`;
            }.bind(this))
        }
    `;
  }

  getProjects() {
    const url = this.host + "/users/1/projects";
    $.ajax('GET', url, function(response) {
        this.projects = response;
    }.bind(this));
}

deleteProject(e) { 	
    const url = this.host + "/projects/" + e.id;
    $.ajax('DELETE', url, function(response){
        this.getProjects();
    }.bind(this));
}

}

customElements.define('list-projects', ListProjects);