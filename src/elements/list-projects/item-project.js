import { LitElement, html, css } from 'lit-element';
import sandbox from '../../sandbox.js';
import $ from '../../../utils/ajax.js';

class ItemProject extends LitElement {

  static get styles() {
    return css`
      :host {
        font-size: 12px;
      }
      
      .card {
        display:inline-block;
        width: 350px;
        border-radius: 15px;
        width: 400px;
        background-color: #fff;
        margin: 0.5em;
        box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
        margin-bottom: 50px;
      }

      .card:hover{
        border-bottom: 8px solid #01a5a6;
     }
     .card-title {
       text-align: center;
     }

     .progress {
       display: inline-block;
       font-size: 12px;
       margin-left: 320px;
       background-color: #FFF;
       color: blue;
     }
    `;
  }

	//Definicion de propiedades
	static get properties() {
    return {
      id: { type: Number },
      sda: { type: String },
      name: { type: String },
      image: { type: String },
      description: { type: String },
      progress: { type: Object }
    };
  }

  constructor() {
    super();
    this.id = 0;
    this.sda = "";
    this.name = "";
    this.image = "";
    this.description = "";
    this.progress = {};
  }

  render() {

    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    	<div class="card">
        <!-- ID de la iniciativa -->
        <h5 class="card-title">${this.sda}</h5>
        <span class="progress">${this.progress.actual}/${this.progress.total}</span>
        <!-- Titulo de la iniciativa -->
        <h5 class="card-title">${this.name}</h5>

        <!-- Imagen de la iniciativa -->
        <img class="card-img-top" src="./img/${this.image}" >

        <!-- Descripcion de la iniciativa -->
        <h5 class="description">${this.description}</h5>
  		<div class="card-body">
		  <p class="card-text"></p>
      		<a href="#" class="btn btn-primary" @click=${this.deleteProject}>Eliminar</a>
		      <a href="/modificar" class="btn btn-primary" @click=${this.showInformation}>Detalle</a>
  		</div>
		</div>
    `;
  }

  deleteProject(){
    const data = {
      id: this.id,
      sda: this.sda
    }
    sandbox.dispatch('delete-project', data , this);
  }

  showInformation(){
<<<<<<< HEAD
    const data = {
      id: this.id,
      sda: this.sda
    }
    console.log(data);
    console.log("Emisor");
    sandbox.dispatch('show-information-project', data , this);
=======
    const url = window.AppConfig.host + "/projects/" + this.id;
    $.ajax('GET', url, function(response) {
      response.modify = true;
      sandbox.dispatch('show-information-project', response , this);
      console.log(response);
    }.bind(this)); 
>>>>>>> f963dc781fcfd89d3ca3cd29ee4247476b922ca8
  }
}

customElements.define('item-project', ItemProject);