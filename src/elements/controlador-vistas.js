import { LitElement, html, css } from 'lit-element';
import {Router, } from '@vaadin/router';
import './checklist-app/checklist-header.js';
import './checklist-app/checklist-home';
import './info-card.js';
import './my-footer.js';
import './list-items/list-items.js';
import './checklist-sign/checklist-login.js';
import './checklist-sign/checklist-register.js';
import './list-projects/list-projects';

import sandbox from '../sandbox.js';

export class ControladorVistas extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor(){
        super();
        this.listenerEvents();
    }

    static styles = css`
        *{
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }
        .app{
            display: none;
        }
        #views > .leaving {
            animation: 1s fadeOut ease-in-out;
        }
        #views > .entering {
            animation: 1s fadeIn linear;
        }
        @keyframes fadeOut {
            from {opacity: 1;}
            to {opacity: 0;}
        }
        @keyframes fadeIn {
            from {opacity: 0;}
            to {opacity: 1;}
        }   
    `;

    firstUpdated(changedProperties) {
        this.routerMain();
    }  

    render() {
        return html`
          <checklist-header></checklist-header>
          <main id="views"></main>
          <my-footer></my-footer>
        `;
    }

    routerMain(){
        const outlet = this.shadowRoot.getElementById('views');
        const router = new Router(outlet);
        router.setRoutes([
          { path: '/', component: 'checklist-login' },
          { path: '/home', component: 'list-projects' },
          { path: '/agregar', component: 'info-card' },
          { path: '/register-user', component: 'checklist-register' },
          { path: '/modificar', component: 'info-card' }
        ]);
    } 

    listenerEvents() {
        sandbox.on('login-ok', this.loginOk.bind(this));
        sandbox.on('login-error', this.loginError.bind(this));
        sandbox.on('register-error', this.registerError.bind(this));
        sandbox.on('session-close', this.sessionClose.bind(this));
      }
    
      loginOk(e) {
        if (e.accessToken) {
          sessionStorage.setItem('accessToken', e.accessToken);
        }
        sessionStorage.setItem('id', e.user.id);
        Router.go("home");
        sandbox.dispatch('send-user-information', e, this);
      }

      sessionClose() {
        sessionStorage.clear();
        Router.go("/");
      }
    
      loginError(e) {
        alert("Los datos introducidos son incorrectos o el usuario no existe.");
      }
    
      registerError(e) {
        alert(e + ", please validate.");
      }      
}
customElements.define('controlador-vistas', ControladorVistas);