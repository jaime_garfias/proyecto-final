# Instalar json-server y/o json-server-auth

## NPM
```bash
npm install -g json-server json-server-auth
```
## Yarn
```bash
yarn add -D json-server json-server-auth
```

### Es posible que necesites instalar express:
```bash
npm install -g express
```
### Correr json-server-auth con puerto default 3000:
```bash
json-server-auth db.json 
```
### Correr json-server-auth con otro puerto:
```bash
json-server-auth db.json -p 4000
```
### Si necesitas aplicar permisos a los json:
```bash
json-server-auth db.json -r routes.json
```
## Documentación
[json-server-auth](https://www.npmjs.com/package/json-server-auth)


## License
